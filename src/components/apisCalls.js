import axios from "axios";

export const fetchBoardsDetails = () => {
  const url =
    "https://api.trello.com/1/members/me/boards?key=f6560517d4c1e4b41ef2e2e625dbfefb&token=0a4d0bf9f84ab0f4c03c4ecd397843911df9105c03e983cb4643dc480ca9f535";
  return axios.get(url);
};

export const addNewBoards = (enteredBoardName) => {
  const addBoardUrl = `https://api.trello.com/1/boards/?name=${enteredBoardName}&key=f6560517d4c1e4b41ef2e2e625dbfefb&token=0a4d0bf9f84ab0f4c03c4ecd397843911df9105c03e983cb4643dc480ca9f535`;
  if (enteredBoardName) {
    return axios.post(addBoardUrl);
  }
};

export const fetchLists = (requiredBoardId) => {
  const getListsDetailsUrl = `https://api.trello.com/1/boards/${requiredBoardId}/lists?key=f6560517d4c1e4b41ef2e2e625dbfefb&token=0a4d0bf9f84ab0f4c03c4ecd397843911df9105c03e983cb4643dc480ca9f535`;
  return axios.get(getListsDetailsUrl);
};

export const addNewList = (boardId, EnteredlistName) => {
  const addNewListUrl = `https://api.trello.com/1/boards/${boardId}/lists?name=${EnteredlistName}&key=f6560517d4c1e4b41ef2e2e625dbfefb&token=0a4d0bf9f84ab0f4c03c4ecd397843911df9105c03e983cb4643dc480ca9f535`;
  if (EnteredlistName) {
    return axios.post(addNewListUrl);
  }
};

export const deleteList = (listId) => {
  const deleteListUrl = `https://api.trello.com/1/lists/${listId}/closed?value=true&key=f6560517d4c1e4b41ef2e2e625dbfefb&token=0a4d0bf9f84ab0f4c03c4ecd397843911df9105c03e983cb4643dc480ca9f535`;
  return axios.put(deleteListUrl);
};

export const getCardsOfList = (listId) => {
  const getCardsUrl = `https://api.trello.com/1/lists/${listId}/cards?key=f6560517d4c1e4b41ef2e2e625dbfefb&token=0a4d0bf9f84ab0f4c03c4ecd397843911df9105c03e983cb4643dc480ca9f535`;
  return axios.get(getCardsUrl);
};
