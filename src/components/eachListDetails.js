import React, { Component } from "react";
import deleteIcon from "../images/delete.svg";
import { deleteList } from "./apisCalls";
import {
  deleteList as deleteListAction,
  displayAllCards,
} from "../redux/actions";
import { connect } from "react-redux";
import { getCardsOfList } from "./apisCalls";

class EachListDetails extends Component {
  componentDidMount = () => {
    getCardsOfList(this.props.eachListDetailsneeded.id)
      .then((res) => res.data)
      .then((data) => {
        this.setState(() => {
          console.log(data);
          this.props.dispatch(
            displayAllCards(this.props.eachListDetailsneeded.id, data)
          );
        });
      });
  };

  handleListDelete = () => {
    deleteList(this.props.eachListDetailsneeded.id)
      .then((res) => res.data.id)
      .then((id) =>
        this.setState(() => this.props.dispatch(deleteListAction(id)))
      );
  };
  render() {
    return (
      <div>
        {console.log(
          `id from each list comp: ${this.props.eachListDetailsneeded.id}`
        )}
        <h1>
          {this.props.eachListDetailsneeded.name}{" "}
          <img
            src={deleteIcon}
            alt="delete"
            id={this.props.eachListDetailsneeded.id}
            style={{ cursor: "pointer" }}
            onClick={this.handleListDelete}
          />
        </h1>
        {/* {this.props.cardsDetails[this.props.eachListDetailsneeded.id] ===undefined? null : this.props.cardsDetails[this.props.eachListDetailsneeded.id].map((EachCard) => (
            <DetailedEachCard
              key={EachCard.id}
              cardDetails={EachCard}
              listDetails={this.props.eachListDetailsneeded}
            />
          ))} */}

      </div>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    cardsDetails: state.cardsReducer,
  };
};

export default connect(mapStateToProps)(EachListDetails);
