import React, { Component } from "react";
import { Link } from "react-router-dom";
import { Card, Modal, Input, Row, Col } from "antd";
import { fetchBoardsDetails, addNewBoards } from "./apisCalls";
import { connect } from "react-redux";

import { displayAllBoards, addNewBoard } from "../redux/actions";
class Boards extends Component {
  state = {
    isModalNeedsToBePoppedUp: false,
    newBoardName: "",
  };
  componentDidMount = () => {
    fetchBoardsDetails().then((res) => {
      this.setState(() => this.props.dispatch(displayAllBoards(res.data)));
    });
  };

  showModal = () => {
    console.log("Show Modal Clicked");
    this.setState(
      {
        isModalNeedsToBePoppedUp: true,
      },
      () =>
        console.log(`at model click: ${this.state.isModalNeedsToBePoppedUp}`)
    );
  };

  handleOk = () => {
    this.addingNewBoard();
    console.log("Ok Clicked");
    this.setState(
      {
        isModalNeedsToBePoppedUp: false,
        newBoardName: "",
      },
      () =>
        console.log(
          `at Ok: ${this.state.isModalNeedsToBePoppedUp},board name in state: ${this.state.newBoardName}`
        )
    );
  };

  handleCancel = () => {
    console.log("Cancel Clicked");
    this.setState(
      {
        isModalNeedsToBePoppedUp: false,
        newBoardName: "",
      },
      () => console.log(`at cancel: ${this.state.isModalNeedsToBePoppedUp}`)
    );
  };

  handleNewBoardName = (event) => {
    this.setState(
      {
        newBoardName: event.target.value,
      },
      () => console.log(`Updated board title:${this.state.newBoardName}`)
    );
  };

  addingNewBoard = () => {
    this.props.boardsDetails.length === 10
      ? alert("Boards limit exceeded")
      : addNewBoards(this.state.newBoardName)
          .then((res) => res.data)
          .then((data) => {
            this.setState(
              {
                isModalNeedsToBePoppedUp: false,
                newBoardName: "",
              },
              () => this.props.dispatch(addNewBoard(data))
            );
          });
  };

  render() {
    return (
      <div>
        <Modal
          title="Add new board"
          visible={this.state.isModalNeedsToBePoppedUp}
          onOk={this.handleOk}
          onCancel={this.handleCancel}
        >
          {" "}
          <Input
            value={this.state.newBoardName}
            onChange={this.handleNewBoardName}
            placeholder="Enter new board name"
          />
        </Modal>
        {this.props.boardsDetails !== [] ? (
          <div>
            <Row>
              {this.props.boardsDetails.map((eachitem) => {
                return (
                  <Col xs={24} xl={4}>
                    <Link to={`/boards/${eachitem.id}`} key={eachitem.id}>
                      <Card style={{ margin: "1rem" }}>
                        {console.log(eachitem.id)}
                        <h6>{eachitem.name}</h6>
                      </Card>
                    </Link>
                  </Col>
                );
              })}

              <Card
                onClick={this.showModal}
                style={{ margin: "1rem", cursor: "pointer" }}
              >
                <h6>Create New Board</h6>
                <h6>Boards left:{10 - this.props.boardsDetails.length}</h6>
              </Card>
            </Row>
          </div>
        ) : (
          <spin size="large" />
        )}
      </div>
    );
  }
}
const mapStateToProps = (state) => ({
  boardsDetails: state.boardsDetailsReducer,
});
export default connect(mapStateToProps)(Boards);
