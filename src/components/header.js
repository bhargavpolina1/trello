import React, { Component } from "react";
import { PageHeader } from "antd";
import "./header.css";

class Header extends Component {
  render() {
    return (
      <PageHeader
        className="site-page-header"
        title="Trello"
        style={{ backgroundColor: "blue" }}
        extra={[<span style={{cursor:"pointer"}}>Home</span>]}
      ></PageHeader>
    );
  }
}

export default Header;
