import "antd/dist/antd.css";
import React, { Component } from "react";
import { fetchLists } from "./apisCalls";
import { displayAllLists } from "../redux/actions";
import { connect } from "react-redux";
import EachListDetails from "./eachListDetails";
import { Col, Row, Card, Modal, Input } from "antd";
import { addNewList } from "./apisCalls";
import { addNewList as addNewListAction } from "../redux/actions";

class DetailedBoardComponent extends Component {
  state = {
    isModalNeedsToBePoppedUp: false,
    newListName: "",
  };
  componentDidMount = () => {
    fetchLists(this.props.match.params.boardId)
      .then((res) => res.data)
      .then((data) => {
        console.log(data);
        this.setState(() => this.props.dispatch(displayAllLists(data)));
      });
  };

  showModal = () => {
    console.log("Show Modal Clicked");
    this.setState(
      {
        isModalNeedsToBePoppedUp: true,
      },
      () =>
        console.log(`at model click: ${this.state.isModalNeedsToBePoppedUp}`)
    );
  };
  handleOk = () => {
    this.addingNewList();
    console.log("Ok Clicked");
    this.setState(
      {
        isModalNeedsToBePoppedUp: false,
        newListName: "",
      },
      () =>
        console.log(
          `at Ok: ${this.state.isModalNeedsToBePoppedUp},List name in state: ${this.state.newListName}`
        )
    );
  };

  handleCancel = () => {
    console.log("Cancel Clicked");
    this.setState(
      {
        isModalNeedsToBePoppedUp: false,
        newListName: "",
      },
      () => console.log(`at cancel: ${this.state.isModalNeedsToBePoppedUp}`)
    );
  };

  handleNewListName = (event) => {
    this.setState(
      {
        newListName: event.target.value,
      },
      () => console.log(`Updated list title:${this.state.newListName}`)
    );
  };

  addingNewList = () => {
    addNewList(this.props.match.params.boardId, this.state.newListName)
      .then((res) => res.data)
      .then((data) => {
        this.setState(
          {
            isModalNeedsToBePoppedUp: false,
            newListName: "",
          },
          () => this.props.dispatch(addNewListAction(data))
        );
      });
  };

  render() {
    return (
      <div>
        <Modal
          title="Add new list"
          visible={this.state.isModalNeedsToBePoppedUp}
          onOk={this.handleOk}
          onCancel={this.handleCancel}
        >
          {" "}
          <Input
            value={this.state.newListName}
            onChange={this.handleNewListName}
            placeholder="Enter new list name"
          />
        </Modal>
        {this.props.listsDetails !== [] ? (
          <div>
            <Row>
              {this.props.listsDetails.map((eachitem) => {
                return (
                  <Col key={eachitem.id}>
                    <Card style={{ margin: ".5rem" }}>
                      <EachListDetails eachListDetailsneeded = {eachitem}/>
                    </Card>
                  </Col>
                );
              })}
              <Col>
                <Card
                  onClick={this.showModal}
                  style={{ margin: ".5rem", cursor: "pointer" }}
                >
                  <h1>Add new list</h1>
                </Card>
              </Col>
            </Row>
          </div>
        ) : (
          <div>Fetching Boards</div>
        )}
      </div>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    listsDetails: state.listsReducer,
  };
};

export default connect(mapStateToProps)(DetailedBoardComponent);
