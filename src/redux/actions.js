export const displayAllBoards = (boardsDetails) => {
  return {
    type: "VIEWALLBOARDS",
    payload: boardsDetails,
  };
};

export const addNewBoard = (newBoard) => {
  return {
    type: "ADDNEWBOARDS",
    payload: newBoard,
  };
};

export const displayAllLists = (listDetails) => {
  return {
    type: "DISPLAYALLLISTS",
    payload: listDetails,
  };
};

export const addNewList = (newListDetail) => {
  return {
    type: "ADDNEWLIST",
    payload: newListDetail,
  };
};

export const deleteList = (listId) => {
  return {
    type: "DELETELIST",
    payload: listId,
  };
};

export const displayAllCards = (listId, cardsData) => {
  return {
    type: "DISPLAYALLCARDS",
    id: listId,
    payload: cardsData,
  };
};
