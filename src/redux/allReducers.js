import { boardsDetailsReducer, listsReducer } from "./reducer";
import { combineReducers } from "redux";

const allReducers = combineReducers({
  boardsDetailsReducer,
  listsReducer,
});

export default allReducers;
