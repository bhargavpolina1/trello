//import axios from "axios";

export const boardsDetailsReducer = (boards = [], action) => {
  switch (action.type) {
    case "VIEWALLBOARDS":
      return [...action.payload];
    case "ADDNEWBOARDS":
      return [...boards, action.payload];
    default:
      return boards;
  }
};

export const listsReducer = (lists = [], action) => {
  switch (action.type) {
    case "DISPLAYALLLISTS":
      return [...action.payload];
    case "ADDNEWLIST":
      return [...lists, action.payload];
    case "DELETELIST":
      return lists.filter((Eachlist) => Eachlist.id !== action.payload);
    default:
      return lists;
  }
};

export const cardsReducer = (state = {}, action) => {
  switch (action.type) {
    case "DISPLAYALLCARDS":
      return { ...state, [action.id]: action.payload };
    default:
      return state;
  }
};


