import React, { Component } from "react";
import "./App.css";
import Boards from "./components/boards";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import Header from "./components/header";
import DetailedBoardComponent from "./components/detailedBoardComponent";



class App extends Component {
  render() {
    return (
      <div className="App">
        <Header />
        <BrowserRouter>
          <Switch>
            <Route exact path="/" component={Boards}></Route>
            <Route exact path="/boards/:boardId" component={DetailedBoardComponent}></Route>
          </Switch>
        </BrowserRouter>
      </div>
    );
  }
}

export default App;
